package com.classshifu.department.controller;

import com.classshifu.department.entity.Department;
import com.classshifu.department.service.DepartmentService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/department")
@Slf4j
public class DepartementController {

    @Autowired
    private DepartmentService departmentService;


    @PostMapping("/")
    public Department saveDepartment(@RequestBody Department department){
        log.info("Inside saveDepartement method of department");
        return departmentService.saveDepartement(department);


    }

    @GetMapping("/{id}")
    public Department findDepartmentById(@PathVariable("id") Long departmentId){
        log.info("Return findDepartmentById method of department");
        return departmentService.findDepartmentById(departmentId);
    }
}
