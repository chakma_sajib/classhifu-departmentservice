package com.classshifu.department.service;

import com.classshifu.department.entity.Department;
import com.classshifu.department.repository.DepartmentRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@Slf4j
public class DepartmentService {

    @Autowired
    private DepartmentRepository departmentRepository;

    public Department saveDepartement(Department department) {
        log.info("Inside saveDepartement of Department Service");
        return departmentRepository.save(department);
    }


    public Department findDepartmentById(Long departmentId) {
        log.info("Inside findDepartmentById of Department Service");
        return departmentRepository.findByDepartmentId(departmentId);

    }
}
